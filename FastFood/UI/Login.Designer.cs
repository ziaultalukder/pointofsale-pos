﻿namespace FastFood.Models
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.bunifuThinButton21 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.loginBunifuThinButton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.passwordBunifuTextbox = new Bunifu.Framework.UI.BunifuTextbox();
            this.forgotBunifuThinButton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.emailBunifuTextbox = new Bunifu.Framework.UI.BunifuTextbox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.crossPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuThinButton21
            // 
            this.bunifuThinButton21.ActiveBorderThickness = 1;
            this.bunifuThinButton21.ActiveCornerRadius = 20;
            this.bunifuThinButton21.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.bunifuThinButton21.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton21.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.bunifuThinButton21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.bunifuThinButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton21.BackgroundImage")));
            this.bunifuThinButton21.ButtonText = "ThinButton";
            this.bunifuThinButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton21.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton21.ForeColor = System.Drawing.Color.White;
            this.bunifuThinButton21.IdleBorderThickness = 1;
            this.bunifuThinButton21.IdleCornerRadius = 20;
            this.bunifuThinButton21.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(68)))), ((int)(((byte)(173)))));
            this.bunifuThinButton21.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.bunifuThinButton21.IdleLineColor = System.Drawing.Color.White;
            this.bunifuThinButton21.Location = new System.Drawing.Point(69, 656);
            this.bunifuThinButton21.Margin = new System.Windows.Forms.Padding(7);
            this.bunifuThinButton21.Name = "bunifuThinButton21";
            this.bunifuThinButton21.Size = new System.Drawing.Size(179, 103);
            this.bunifuThinButton21.TabIndex = 0;
            this.bunifuThinButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // loginBunifuThinButton
            // 
            this.loginBunifuThinButton.ActiveBorderThickness = 1;
            this.loginBunifuThinButton.ActiveCornerRadius = 20;
            this.loginBunifuThinButton.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.loginBunifuThinButton.ActiveForecolor = System.Drawing.Color.White;
            this.loginBunifuThinButton.ActiveLineColor = System.Drawing.Color.Lime;
            this.loginBunifuThinButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.loginBunifuThinButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("loginBunifuThinButton.BackgroundImage")));
            this.loginBunifuThinButton.ButtonText = "Log-In";
            this.loginBunifuThinButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.loginBunifuThinButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginBunifuThinButton.ForeColor = System.Drawing.Color.White;
            this.loginBunifuThinButton.IdleBorderThickness = 1;
            this.loginBunifuThinButton.IdleCornerRadius = 20;
            this.loginBunifuThinButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.loginBunifuThinButton.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.loginBunifuThinButton.IdleLineColor = System.Drawing.Color.White;
            this.loginBunifuThinButton.Location = new System.Drawing.Point(94, 366);
            this.loginBunifuThinButton.Margin = new System.Windows.Forms.Padding(5);
            this.loginBunifuThinButton.Name = "loginBunifuThinButton";
            this.loginBunifuThinButton.Size = new System.Drawing.Size(91, 57);
            this.loginBunifuThinButton.TabIndex = 0;
            this.loginBunifuThinButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.loginBunifuThinButton.Click += new System.EventHandler(this.loginBunifuThinButton_Click);
            // 
            // passwordBunifuTextbox
            // 
            this.passwordBunifuTextbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.passwordBunifuTextbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("passwordBunifuTextbox.BackgroundImage")));
            this.passwordBunifuTextbox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.passwordBunifuTextbox.ForeColor = System.Drawing.Color.White;
            this.passwordBunifuTextbox.Icon = ((System.Drawing.Image)(resources.GetObject("passwordBunifuTextbox.Icon")));
            this.passwordBunifuTextbox.Location = new System.Drawing.Point(63, 294);
            this.passwordBunifuTextbox.Name = "passwordBunifuTextbox";
            this.passwordBunifuTextbox.Size = new System.Drawing.Size(312, 54);
            this.passwordBunifuTextbox.TabIndex = 1;
            this.passwordBunifuTextbox.text = "   Password";
            // 
            // forgotBunifuThinButton
            // 
            this.forgotBunifuThinButton.ActiveBorderThickness = 1;
            this.forgotBunifuThinButton.ActiveCornerRadius = 20;
            this.forgotBunifuThinButton.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.forgotBunifuThinButton.ActiveForecolor = System.Drawing.Color.White;
            this.forgotBunifuThinButton.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.forgotBunifuThinButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.forgotBunifuThinButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("forgotBunifuThinButton.BackgroundImage")));
            this.forgotBunifuThinButton.ButtonText = "Forgot Password";
            this.forgotBunifuThinButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.forgotBunifuThinButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forgotBunifuThinButton.ForeColor = System.Drawing.Color.White;
            this.forgotBunifuThinButton.IdleBorderThickness = 1;
            this.forgotBunifuThinButton.IdleCornerRadius = 20;
            this.forgotBunifuThinButton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.forgotBunifuThinButton.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.forgotBunifuThinButton.IdleLineColor = System.Drawing.Color.White;
            this.forgotBunifuThinButton.Location = new System.Drawing.Point(202, 366);
            this.forgotBunifuThinButton.Margin = new System.Windows.Forms.Padding(5);
            this.forgotBunifuThinButton.Name = "forgotBunifuThinButton";
            this.forgotBunifuThinButton.Size = new System.Drawing.Size(150, 57);
            this.forgotBunifuThinButton.TabIndex = 0;
            this.forgotBunifuThinButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // emailBunifuTextbox
            // 
            this.emailBunifuTextbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.emailBunifuTextbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("emailBunifuTextbox.BackgroundImage")));
            this.emailBunifuTextbox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.emailBunifuTextbox.ForeColor = System.Drawing.Color.White;
            this.emailBunifuTextbox.Icon = ((System.Drawing.Image)(resources.GetObject("emailBunifuTextbox.Icon")));
            this.emailBunifuTextbox.Location = new System.Drawing.Point(63, 220);
            this.emailBunifuTextbox.Name = "emailBunifuTextbox";
            this.emailBunifuTextbox.Size = new System.Drawing.Size(312, 54);
            this.emailBunifuTextbox.TabIndex = 1;
            this.emailBunifuTextbox.text = "   Email";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(141, 85);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(158, 127);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.crossPanel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(440, 34);
            this.panel1.TabIndex = 3;
            // 
            // crossPanel
            // 
            this.crossPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("crossPanel.BackgroundImage")));
            this.crossPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.crossPanel.Location = new System.Drawing.Point(403, 2);
            this.crossPanel.Name = "crossPanel";
            this.crossPanel.Size = new System.Drawing.Size(33, 31);
            this.crossPanel.TabIndex = 4;
            this.crossPanel.Click += new System.EventHandler(this.crossPanel_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.ClientSize = new System.Drawing.Size(440, 466);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.emailBunifuTextbox);
            this.Controls.Add(this.passwordBunifuTextbox);
            this.Controls.Add(this.forgotBunifuThinButton);
            this.Controls.Add(this.loginBunifuThinButton);
            this.Controls.Add(this.bunifuThinButton21);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton21;
        private Bunifu.Framework.UI.BunifuThinButton2 loginBunifuThinButton;
        private Bunifu.Framework.UI.BunifuTextbox passwordBunifuTextbox;
        private Bunifu.Framework.UI.BunifuThinButton2 forgotBunifuThinButton;
        private Bunifu.Framework.UI.BunifuTextbox emailBunifuTextbox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel crossPanel;
    }
}