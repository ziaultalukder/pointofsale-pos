﻿using System;
using System.Windows.Forms;
using FastFood.Models;

namespace FastFood
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            sidePanel.Height = homeButton.Height;
            sidePanel.Top = homeButton.Top;
            firstCustomerControl1.BringToFront();
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            sidePanel.Height = homeButton.Height;
            sidePanel.Top = homeButton.Top;
            firstCustomerControl1.BringToFront();
        }
        

        private void exitButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are You Sure You Want To Exit", "Please Confirm",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        

        private void loginButton_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
        }
        

        private void SetupButton_Click(object sender, EventArgs e)
        {
            sidePanel.Height = SetupButton.Height;
            sidePanel.Top = SetupButton.Top;
            setupControll1.BringToFront();
        }

        private void productButton_Click(object sender, EventArgs e)
        {
            sidePanel.Height = productButton.Height;
            sidePanel.Top = productButton.Top;
            stock1.BringToFront();
        }
        private void salesButton_Click(object sender, EventArgs e)
        {
            sidePanel.Height = salesButton.Height;
            sidePanel.Top = salesButton.Top;
            sales1.BringToFront();
        }

        private void stockButton_Click(object sender, EventArgs e)
        {
            sidePanel.Height = stockButton.Height;
            sidePanel.Top = stockButton.Top;
            stock1.BringToFront();
        }

        
    }
}
